param([string]$path)

$subdirs = "content", "data", "bin", "bib"

foreach($subdir in $subdirs)
{
	# Test existence of directory
	$target = Join-Path $path $subdir
	If(Test-Path -Path $target)
	{
		Write-Host "$target already exists!" -ForegroundColor Red
	}
	Else
	{
		New-Item $target  -type directory
	}
}

# Download Tex.gitignore for this project
$client = New-Object System.Net.WebClient
$texIgnoreURL = "https://raw.githubusercontent.com/github/gitignore/master/TeX.gitignore"
$client.DownloadFile($texIgnoreURL, [System.IO.Path]::Combine($path, ".gitignore"))

# Create main.tex file
New-Item (Join-Path -Path $path "main.tex")